# Readme.txt

1. Installation

	After recieving the download, you should have 5 files named:

	1. Blackjack 2 players
	2. Cards.txt
	3. Score.htb
	4. Numbers.txt
	5. Readme.txt

	The only filename you can change is the file 'Blackjack 2 players' and the only file which you can clear of all information is 'score.htb', which would reset the scores.

	Save the files in any directory. All the files MUST be in the same directory otherwise they will not work.

1. Loading

	1. Load up 'mirc.exe'. Hold down the alt and push 'r' to open up the script editor. 
	2. Select the Variables tab; unless something is there and you need it, delete it. Please read the section on variables later on if you have an important script that requires the use of global variables.
	3. Now hit the Remote tab. Hold down ctrl and hit 'l'. Select the file 'Blackjack 2 players' from the appropriate directory and hit 'ok'.

3. Playing

	Now that the file is loaded, go to the channel where you want to start a game. On the main screen type '/blackjack_setup #channel' substitute #channel for the channel you want your games to start in. For example, '/blackjack_setup #pokebattle'. Then in that channel type '/blackjack' and a game begins.

4. Variables

	Be advised that the only variables being used are:

	1. %players
	2. %bet
	3. %deck
	4. %channel
	5. There are also other variables that change depending on the game. From example, if Bob and Mary were playing, there would be global variables starting with %bob... and %mary...

## Notes:

Whenever you clear global variables, or if ever %channel is missing, blackjack script will not work. If you want to change the channel where you are playing your game, re-type '/blackjack_setup #channel', subsituting #channels with the channel of your choice. Because of saftey precautions (and to stop blackjack messages from flying from one channel to another), all the messages will be sent to #channel.

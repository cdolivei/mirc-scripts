# ABOUT

This repository contains some mIRC scripts that I haven't touched for at least a decade.
I wrote more, but it seems most of it went missing.
But maybe someone will find it useful, even as an educational tool.

I decided to upload this after someone I knew was demeaned for uploading code/tools that someone else thought was garbage. We all write garbage code at one time or another. This is my garbage code, uploaded proudly.

# LICENCE

All code is available under Public Domain. You are free to do whatever you want with it without my permission and without giving me credit
